from armors import *
from runes import *
default_primary_stat = 1000
# secondary is 0 standard
# Health pools
# Warrior, Necromancer
WN_HP = 9212
# Revenant, Engineer, Ranger, Mesmer
RERM_HP = 5922
#Guardian, Thief, Elementalist
GTE_HP = 1645

# TODO: when to add powersharpening stones/...?

# armorset is a list of ArmorPieces
def life(proffession_hp,armorset,food,utility):
    life = proffession_hp + 10000 # Base vitality = 1000
    for armor in armorset:
        life += armor.return_stat(vitality)*10
    return life

def ehp(proffession_hp,armorset,food,utility):
    hp = life(proffession_hp,armorset)
    total_toughness = 1000 # Base toughness = 1000
    for armor in armorset:
        total_toughness += armor.return_stat(toughness)
    return hp*(total_toughness / 1000.0 + 0.967) / 1.967;

# TODO: food/utility implementation
def dmg(proffession_hp,armorset,food,utility):
    total_power = 1000;
    total_precision = 1000;
    ferocity = 0;
    for armor in armorset:
        total_power += armor.return_stat(power)
    for armor in armorset:
        total_precision += armor.return_stat(power)
    for armor in armorset:
        total_precision += armor.return_stat(ferocity)
    G5 = power / 1000; #power multiplier
    H5 = ((precision - 916) / 21) / 100; #critchance
    I5 = 1.5 + (1.0 / 15.0 * ferocity) / 100; #critmultiplier

    #dmg with 12 might stacks and 50% fury upkeep
    dmg = 0.5 * ((G5 + 12.0 * 0.03)*(H5 + 0.2)*I5 + (G5 + 12.0 * 0.03)*(1.0 - (H5 + 0.02))) + 0.5 * ((G5 + 12.0 * 0.03)*(H5)*I5 + (G5 + 12.0 * 0.03)*(1.0 - H5));
    return dmg
