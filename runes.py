from runedistr import *

class Runeset():
    # Runesets will change the stats on armor and supply extra stats
    def __init__(self,stats,stat_numbers, runename):
        # Runes' flat bonuses
        self.stat_numbers = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        for i in range(len(stats)):
            self.stat_numbers[stats[i]] = stat_numbers[i]
        self.runename = runename

        def return_stat(self,stat):
            return stats[stat]


all_rune_distr = [pow_tough,pow_prec,pow_condd,pow_fer_perc_dmg,pow_cond_d,\
pow_tough_dmgred,pow_,prec_fer,prec_fer2,prec_condd3,tough_vit,tough_d,\
tough_vit,vit_vittoother,vit_d,vit_tough,vit_pow,vit_fer,fer_pow,fer_prec,\
fer_d,condd_pow,condd,condd_vit,condd_tough,heal_vit,allstat,infiltr]

all_runes = [Runeset(distr[0],distr[1],distr[2]) for distr in all_rune_distr]
