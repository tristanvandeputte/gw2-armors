from glbl import *
# =========================Power Runes======================================
pow_tough = ([power,toughness],[175,100],"brawler/daredevil/reaper")
pow_prec = ([power,precision],[175,125],"pack")
pow_condd = ([power,condition_damage],[175,100],"elementalist")
pow_fer_perc_dmg = ([power,ferocity,percent_damage],[175,35,4],"ogre")
# ATTENTION: scholar runes require > 90% hp
pow_cond_d = ([power,ferocity,percent_damage],[175,100,10],"scholar")
# ATTENTION: scrapper runes require > 600 range
pow_tough_dmgred = ([power,toughness,percent_damage_red],[175,100,7],"scrapper")
# ATTENTION: strength runes require might
pow_ = ([power,percent_damage],[175,5],"strength")

# =========================Precision Runes==============================
# ATTENTION: eagle runes require enemy < 50% hp
prec_fer = ([precision,ferocity,percent_damage],[175,100,6],"eagle")
# ATTENTION: ranger with companion only TODO: check for this
prec_fer2 = ([precision,ferocity,percent_damage],[175,100,6],"ranger")
# ATTENTION: only when attacking from behind
prec_condd3 = ([precision,condition_damage,percent_damage],[175,100,10],"thief")

# =========================Toughness Runes==============================
tough_vit = ([toughness,vitality],[175,125],"durability") # boon dur too
# ATTENTION: While wielding a conjured weapon, environmental weapons, kits, banners, and stolen items
tough_d = ([toughness,condition_damage,percent_damage],[175,100,7],"engineer")
tough_vit = ([toughness,healing],[175,100],"guardian")

# =========================Vitality Runes==============================
vit_vittoother = ([vitality,percent_vit_to_heal,percent_vit_to_prec,percent_vit_to_pow],[175,100,3,5,7],"exuberance")
#ATTENTION: vs chilled enemies
vit_d = ([vitality,percent_damage],[175,7],"ice")
vit_tough = ([vitality,toughness],[175,100],"trooper")
vit_pow = ([vitality,power],[175,100],"warrior")
vit_fer = ([vitality,ferocity,percent_vit_to_fer],[175,100,7],"wurm")

# =========================Ferocity Runes==============================
fer_pow = ([ferocity,power],[175,100],"dragonhunter")
fer_prec = ([ferocity,precision],[175,100],"golemancer")
#ATTENTION: if you have fury
fer_d = ([ferocity,percent_damage],[175,5],"rage")

# =========================Condition Damage==============================
condd_pow = ([condition_damage,power,percent_damage],[175,100,5],"berserker")
condd = ([condition_damage],[183],"antitoxin")
condd_vit = ([condition_damage,vitality],[175,100],"necromancer")
condd_tough = ([condition_damage,toughness,percent_tough_to_condd],[175,100,7],"undead")
# =========================Healing Power==============================
heal_vit = ([healing,vitality],[175,100],"druid")

# ============================Others===================================
# TODO: allstat only these?
allstat = ([power,precision,ferocity,vitality,toughness,healing,condition_damage],[78,78,78,78,78,78,78],"divinity")
# ATTENTION: enemy < 50% hp
infiltr = ([healing,percent_damage],[100,12],"infiltration")
# TODO: rune of life 20 hp-> worthless?
