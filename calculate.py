from logic import *
# This could be considered the "main" of the program

# Determines base HP (dont forget " ")
#profession = input('What is your proffesion?')
# For the below questions, answer 0 for default
#power_min =  int(input('What would you like your Power to be?'))
#precision_min = int(input('What would you like your Precision to be?'))
#ferocity_min = int(input('What would you like your Ferocity to be?'))
#vitality_min = int(input('What would you like your Vitality to be?'))
#toughness_min = int(input('What would you like your Toughness to be?'))
#healing_min = int(input('What would you like your Healing to be?'))
#condition_damage_min = int(input('What would you like your Condition_damage to be?'))
#concentration_min = int(input('What would you like your Concentration to be?'))
#expertise_min = int(input('What would you like your Expertise to be?'))
# Here you can put dmg/ehp/healing/condi dmg/concentration/expertise
# TODO: formula with condi dmg+duration?
# TODO: multiple maximizations?
#to_maximize = input('What attribute would you like to maximize?')

# 5 armor sets, 5 runesets
armors_to_compare = ["berserker","zealot","assassin","cleric","knight"]
#for i in range(5):
#    armors_to_compare.append(input('itemset '+str(i)+': '))
#print armors_to_compare
#weapon_type = input('1-2H?')
runes_to_compare = ["pack"]
#for i in range(5):
#    armors_to_compare.append(input('runemset '+str(i)+': '))
#print armors_to_compare

# make all combinations of the relevant armors/runes
relevant_helmets = [armor_piece for armor_piece in all_helmets if armor_piece.prefix_name in armors_to_compare]
relevant_chests = [armor_piece for armor_piece in all_chests if armor_piece.prefix_name in armors_to_compare]
relevant_shoulders = [armor_piece for armor_piece in all_shoulders if armor_piece.prefix_name in armors_to_compare]
relevant_gloves = [armor_piece for armor_piece in all_gloves if armor_piece.prefix_name in armors_to_compare]
relevant_leggings = [armor_piece for armor_piece in all_leggings if armor_piece.prefix_name in armors_to_compare]
relevant_boots = [armor_piece for armor_piece in all_boots if armor_piece.prefix_name in armors_to_compare]
relevant_1h_weapons = [armor_piece for armor_piece in all_weapons_1h if armor_piece.prefix_name in armors_to_compare]
relevant_2h_weapons = [armor_piece for armor_piece in all_weapons_2h if armor_piece.prefix_name in armors_to_compare]

relevant_runes = [rune for rune in all_runes if rune.runename in runes_to_compare]

# first of all: all of the different item combo's (a ton)
from itertools import product
from copy import copy
allcombos = product(relevant_helmets,relevant_chests,relevant_shoulders,relevant_gloves,relevant_leggings,relevant_boots,all_weapons_2h)

allcombos_with_runes = []
for combo in allcombos:
    total_set = ArmorSet(combo)
    for runeset in relevant_runes:
        allcombos_with_runes.append(total_set.applyRunes(runeset))
# Eliminate those who don't make the minimums


print amount
