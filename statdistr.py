from glbl import *

# ==============================ARMOR======================================
# All different armor pieces with 3 attributes
three_stat_helmet = [63,45,45]
three_stat_shoulders = [47,34,34]
three_stat_chest = [141,101,101]
three_stat_gloves = [47,34,34]
three_stat_leggings = [94,67,67]
three_stat_boots = [47,34,34]
three_stat_weapon_1h = [125,90,90]
three_stat_weapon_2h = [251,179,179]

# All different armor pieces with 4 attributes
four_stat_helmet = [54,54,30,30]
four_stat_shoulders = [40,40,22,22]
four_stat_chest = [121,121,67,67]
four_stat_gloves = [40,40,22,22]
four_stat_leggings = [81,81,44,44]
four_stat_boots = [40,40,22,22]
four_stat_weapon_1h = [108,108,59,59]
four_stat_weapon_2h = [215,215,118,118]

# Special case: Celestial
celestial_helmet = [30,30,30,30,30,30,30]
celestial_shoulders = [22,22,22,22,22,22,22]
celestial_chest = [67,67,67,67,67,67,67]
celestial_gloves = [22,22,22,22,22,22,22]
celestial_leggings = [44,44,44,44,44,44,44]
celestial_boots = [22,22,22,22,22,22,22]
celestial_weapon_1h = [59,59,59,59,59,59,59]
celestial_weapon_2h = [118,118,118,118,118,118,118]

# ============================Trinkets====================================
# IMPORTANT: Only Amulets can have all the different prefixes
# All different armor pieces with 3 attributes
three_stat_accessory = [110,74,74]
three_stat_amulet = [157,108,108]
three_stat_ring = [126,85,85]
three_stat_backpack = [60,40,40]

# All different armor pieces with 4 attributes
#four_stat_accessory = -> TODO:doesn't exist
four_stat_amulet = [133,133,77,77]
# four_stat_ring = [] -> TODO:doesn't exist
# four_stat_backpack = [] -> TODO:doesn't exist

# Mixed 4 stats (zerk/valk),(dire/rabid)
four_stat_accessory_mix1 = [110,74,56,18]
four_stat_amulet_mix1 = [157,108,90,18]
four_stat_ring_mix1 = [126,85,67,18]
# (rabid,apothecary)
four_stat_accessory_mix1 = [96,74,56,32]
four_stat_amulet_mix2 = [143,108,90,32]
four_stat_ring_mix2 = [112,85,67,32]

# Celestial Trinkets
celestial_accessory = [50,50,50,50,50,50,50]
celestial_amulet = [72,72,72,72,72,72,72]
celestial_ring = [57,57,57,57,57,57,57]
#celestial_backpack = [] -> TODO:doesn't exist

# =============================Prefixes======================================
# The different prefixes on armor/trinkets
# 3 attr
berserker = ([power,precision,ferocity],"berserker")
zealot = ([power,precision,healing],"zealot")
soldier = ([power,toughness,vitality],"soldier")
valkyrie = ([power,vitality,ferocity],"valkyrie")
assassin = ([precision,power,ferocity],"assassin")
rampager = ([precision,power,condition_damage],"rampager")
knight = ([toughness,power,precision],"knight")
nomad = ([toughness,vitality,healing],"nomad")
settler = ([toughness,condition_damage,healing],"settler")
sentinel = ([vitality,power,toughness],"sentinel")
shaman = ([vitality,condition_damage,healing],"shaman")
carrion = ([condition_damage,power,vitality],"carrion")
dire = ([condition_damage,toughness,vitality],"dire")
rabid = ([condition_damage,precision,toughness],"rabid")
sinister = ([condition_damage,power,precision],"sinister")
apothecary = ([healing,toughness,condition_damage],"apothecary")
cleric = ([healing,power,toughness],"cleric")
magi = ([healing,precision,vitality],"magi")
# 4 attr
commander = ([power,precision,toughness,concentration],"commander")
marauder = ([power,precision,vitality,ferocity],"marauder")
vigilant = ([power,toughness,concentration,expertise],"vigilant")
crusader = ([power,toughness,ferocity,healing],"crusader")
wanderer = ([power,vitality,toughness,concentration],"wanderer")
trailblazer = ([toughness,condition_damage,vitality,expertise],"trailblazer")
minstrel = ([toughness,healing,vitality,concentration],"minstrel")
# Celestial (written out as the numerical values of the "enum" for readability)
celestial = [0,1,2,3,4,5,6]

# All of the different prefixes
all_3_attr_prefixes = [berserker,zealot,soldier,valkyrie,assassin,rampager,knight,nomad,settler,sentinel,shaman,carrion,dire,rabid,sinister,apothecary,cleric,magi]
all_4_attr_prefixes = [commander,marauder,vigilant,crusader,wanderer,trailblazer,minstrel]
# Celestial doesn't need to be in a list
