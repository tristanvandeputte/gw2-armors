#include <random>
#include <fstream>
#include <stack>
#include <algorithm>
#include <time.h>
#include <iostream>
#include <sstream>
using namespace std;

struct armor {
	int stats[5] = { 0,0,0,0,0 };
	//0=power, 1=prec, 2=fer, 3=vit, 4=toughness
};

struct set {
	armor setpiece[13];
};

armor assign(int x0, int x1, int x2, int x3, int x4) {
	armor myarmor;
	myarmor.stats[0] = x0;
	myarmor.stats[1] = x1;
	myarmor.stats[2] = x2;
	myarmor.stats[3] = x3;
	myarmor.stats[4] = x4;
	return myarmor;
}

double life(set set) {
	double vitality = 1000;
	for (int i = 0; i<13; i++) {
		vitality += set.setpiece[i].stats[3];
	}
	double life = 1645;
	life += vitality * 10;
	return life;
}

double ehp(set set) {
	double ehp;
	double hp = life(set);
	double toughness = 1000;
	for (int i = 0; i<13; i++) {
		toughness += set.setpiece[i].stats[4];
	}
	ehp = hp*(toughness / 1000.0 + 0.967) / 1.967;
	return ehp;

}


double dmg(set set) {
	double power = 1000;
	for (int i = 0; i<13; i++) {
		power += set.setpiece[i].stats[0];
	}
	double precision = 1000;
	for (int i = 0; i<13; i++) {
		precision += set.setpiece[i].stats[1];
	}
	double ferocity = 0;
	for (int i = 0; i<13; i++) {
		ferocity += set.setpiece[i].stats[2];
	}
	//cout << "ppf" << power << " " << precision << " " << ferocity << endl;
	double G5 = power / 1000; //power multiplier
	double H5 = ((precision - 916) / 21) / 100; //critchance
	double I5 = 1.5 + (1.0 / 15.0 * ferocity) / 100; //critmultiplier

	//dmg with 12 might stacks and 50% fury upkeep
	double dmg = 0.5 * ((G5 + 12.0 * 0.03)*(H5 + 0.2)*I5 + (G5 + 12.0 * 0.03)*(1.0 - (H5 + 0.02))) + 0.5 * ((G5 + 12.0 * 0.03)*(H5)*I5 + (G5 + 12.0 * 0.03)*(1.0 - H5));
	return dmg;
}

//dmg with hoelbrak and powersharpening stones
double dmghoel(set set) {

	double vitality = 1000;
	for (int i = 0; i < 13; i++) {
		vitality += set.setpiece[i].stats[3];
	}
	double toughness = 1000;
	for (int i = 0; i < 13; i++) {
		toughness += set.setpiece[i].stats[4];
	}

	double power = 1000;
	for (int i = 0; i < 13; i++) {
		power += set.setpiece[i].stats[0];
	}
	power += 0.06*toughness + 0.04*vitality;
	power += 175;
	double precision = 1000;
	for (int i = 0; i < 13; i++) {
		precision += set.setpiece[i].stats[1];
	}
	double ferocity = 0;
	for (int i = 0; i < 13; i++) {
		ferocity += set.setpiece[i].stats[2];
	}

	//cout << "ppf" << power << " " << precision << " " << ferocity << endl;
	double G5 = power / 1000; //power multiplier
	double H5 = ((precision - 916) / 21) / 100; //critchance
	double I5 = 1.5 + (1.0 / 15.0 * ferocity) / 100; //critmultiplier

	double dmg = 0.5 * ((G5 + 12.0 * 0.03)*(H5 + 0.2)*I5 + (G5 + 12.0 * 0.03)*(1.0 - (H5 + 0.02))) + 0.5 * ((G5 + 12.0 * 0.03)*(H5)*I5 + (G5 + 12.0 * 0.03)*(1.0 - H5));
	dmg = dmg;
	return dmg;
}

//dmg with hoelbrak and powersharpening stones
double dmgstr(set set) {

	double vitality = 1000;
	for (int i = 0; i < 13; i++) {
		vitality += set.setpiece[i].stats[3];
	}
	double toughness = 1000;
	for (int i = 0; i < 13; i++) {
		toughness += set.setpiece[i].stats[4];
	}

	double power = 1000;
	for (int i = 0; i < 13; i++) {
		power += set.setpiece[i].stats[0];
	}
	power += 0.06*toughness + 0.04*vitality;
	power += 175;
	double precision = 1000;
	for (int i = 0; i < 13; i++) {
		precision += set.setpiece[i].stats[1];
	}
	double ferocity = 0;
	for (int i = 0; i < 13; i++) {
		ferocity += set.setpiece[i].stats[2];
	}

	//cout << "ppf" << power << " " << precision << " " << ferocity << endl;
	double G5 = power / 1000; //power multiplier
	double H5 = ((precision - 916) / 21) / 100; //critchance
	double I5 = 1.5 + (1.0 / 15.0 * ferocity) / 100; //critmultiplier

	double dmg = 0.5 * ((G5 + 12.0 * 0.03)*(H5 + 0.2)*I5 + (G5 + 12.0 * 0.03)*(1.0 - (H5 + 0.02))) + 0.5 * ((G5 + 12.0 * 0.03)*(H5)*I5 + (G5 + 12.0 * 0.03)*(1.0 - H5));
	dmg = dmg = dmg*1.05;
	return dmg;
}

//dmg with exu and powersharpening stones
double dmgexu(set set) {

	double vitality = 1000;
	for (int i = 0; i < 13; i++) {
		vitality += set.setpiece[i].stats[3];
	}
	vitality += 175;
	double toughness = 1000;
	for (int i = 0; i < 13; i++) {
		toughness += set.setpiece[i].stats[4];
	}

	double power = 1000;
	for (int i = 0; i < 13; i++) {
		power += set.setpiece[i].stats[0];
	}
	power += 0.06*toughness + 0.04*vitality;
	power += 0.07*vitality;
	double precision = 1000;
	for (int i = 0; i < 13; i++) {
		precision += set.setpiece[i].stats[1];
	}
	precision += 0.05*vitality;
	double ferocity = 0;
	for (int i = 0; i < 13; i++) {
		ferocity += set.setpiece[i].stats[2];
	}

	//cout << "ppf" << power << " " << precision << " " << ferocity << endl;
	double G5 = power / 1000; //power multiplier
	double H5 = ((precision - 916) / 21) / 100; //critchance
	double I5 = 1.5 + (1.0 / 15.0 * ferocity) / 100; //critmultiplier

	double dmg = 0.5 * ((G5 + 12.0 * 0.03)*(H5 + 0.2)*I5 + (G5 + 12.0 * 0.03)*(1.0 - (H5 + 0.02))) + 0.5 * ((G5 + 12.0 * 0.03)*(H5)*I5 + (G5 + 12.0 * 0.03)*(1.0 - H5));
	dmg = dmg;
	return dmg;
}

//life with exuberance
double lifeexu(set set) {
	double vitality = 1000;
	for (int i = 0; i<13; i++) {
		vitality += set.setpiece[i].stats[3];
	}
	vitality += 175;
	double life = 1645;
	life += vitality * 10;
	return life;
}

//ehp with exuberance
double ehpexu(set set) {
	double ehp;
	double hp = lifeexu(set);
	double toughness = 1000;
	for (int i = 0; i<13; i++) {
		toughness += set.setpiece[i].stats[4];
	}
	ehp = hp*(toughness / 1000.0 + 0.967) / 1.967;
	return ehp;

}




int main() {
	double minehp;
	cout << "minehp: ";
	cin >> minehp;

	armor allarmor[13][4];
	//Zerk
	allarmor[0][0] = assign(63, 45, 45, 0, 0);
	allarmor[1][0] = assign(47, 34, 34, 0, 0);
	allarmor[2][0] = assign(141, 101, 101, 0, 0);
	allarmor[3][0] = assign(47, 34, 34, 0, 0);
	allarmor[4][0] = assign(94, 67, 67, 0, 0);
	allarmor[5][0] = assign(47, 34, 34, 0, 0);
	allarmor[6][0] = assign(157, 108, 108, 0, 0);
	allarmor[7][0] = assign(126, 85, 85, 0, 0);
	allarmor[8][0] = assign(126, 85, 85, 0, 0);
	allarmor[9][0] = assign(110, 74, 74, 0, 0);
	allarmor[10][0] = assign(110, 74, 74, 0, 0);
	allarmor[11][0] = assign(63, 40, 40, 0, 0);
	allarmor[12][0] = assign(251, 179, 179, 0, 0);

	//Soldier
	allarmor[0][1] = assign(63, 0, 0, 45, 45);
	allarmor[1][1] = assign(47, 0, 0, 34, 34);
	allarmor[2][1] = assign(141, 0, 0, 101, 101);
	allarmor[3][1] = assign(47, 0, 0, 34, 34);
	allarmor[4][1] = assign(94, 0, 0, 67, 67);
	allarmor[5][1] = assign(47, 0, 0, 34, 34);
	allarmor[6][1] = assign(157, 0, 0, 108, 108);
	allarmor[7][1] = assign(126, 0, 0, 85, 85);
	allarmor[8][1] = assign(126, 0, 0, 85, 85);
	allarmor[9][1] = assign(110, 0, 0, 74, 74);
	allarmor[10][1] = assign(110, 0, 0, 74, 74);
	allarmor[11][1] = assign(63, 0, 0, 40, 40);
	allarmor[12][1] = assign(251, 0, 0, 179, 179);

	//Marauder
	allarmor[0][2] = assign(54, 54, 30, 30, 0);
	allarmor[1][2] = assign(40, 40, 22, 22, 0);
	allarmor[2][2] = assign(121, 121, 67, 67, 0);
	allarmor[3][2] = assign(40, 40, 22, 22, 0);
	allarmor[4][2] = assign(81, 81, 44, 44, 0);
	allarmor[5][2] = assign(40, 40, 22, 22, 0);
	allarmor[6][2] = assign(133, 133, 71, 71, 0);
	allarmor[7][2] = assign(0, 0, 0, 0, 0); //doesnt exist
	allarmor[8][2] = assign(0, 0, 0, 0, 0);
	allarmor[9][2] = assign(0, 0, 0, 0, 0);
	allarmor[10][2] = assign(0, 0, 0, 0, 0);
	allarmor[11][2] = assign(0, 0, 0, 0, 0);
	allarmor[12][2] = assign(215, 215, 118, 118, 0);

	//Valkyrie and berserker+valkyrie trinkets
	allarmor[0][3] = assign(63, 0, 45, 45, 0);
	allarmor[1][3] = assign(47, 0, 34, 34, 0);
	allarmor[2][3] = assign(141, 0, 101, 101, 0);
	allarmor[3][3] = assign(47, 0, 34, 34, 0);
	allarmor[4][3] = assign(94, 0, 67, 67, 0);
	allarmor[5][3] = assign(47, 0, 34, 34, 0);
	allarmor[6][3] = assign(157, 90, 108, 18, 0);
	allarmor[7][3] = assign(126, 67, 85, 18, 0);
	allarmor[8][3] = assign(126, 67, 85, 18, 0);
	allarmor[9][3] = assign(110, 56, 74, 18, 0);
	allarmor[10][3] = assign(110, 56, 74, 18, 0);
	allarmor[11][3] = assign(63, 22, 40, 18, 0);
	allarmor[12][3] = assign(251, 0, 179, 179, 0);


	set currentset;
	set currentbest;
	currentbest.setpiece[0] = allarmor[0][1];
	currentbest.setpiece[1] = allarmor[1][1];
	currentbest.setpiece[2] = allarmor[2][1];
	currentbest.setpiece[3] = allarmor[3][1];
	currentbest.setpiece[4] = allarmor[4][1];
	currentbest.setpiece[5] = allarmor[5][1];
	currentbest.setpiece[6] = allarmor[6][1];
	currentbest.setpiece[7] = allarmor[7][1];
	currentbest.setpiece[8] = allarmor[8][1];
	currentbest.setpiece[9] = allarmor[9][1];
	currentbest.setpiece[10] = allarmor[10][1];
	currentbest.setpiece[11] = allarmor[11][1];
	currentbest.setpiece[12] = allarmor[12][1];

	int best[13];
	for (int i = 1; i<13; i++) {
		best[i] = 1;
	}

	cout << "dmg: " << dmgstr(currentbest) << endl;
	cout << ehp(currentbest) << endl;
	cout << life(currentbest) << endl;

	int n = 4;
	for (int i0 = 0; i0<n; i0++) {
		for (int i1 = 0; i1<n; i1++) {
			for (int i2 = 0; i2<n; i2++) {
				for (int i3 = 0; i3<n; i3++) {
					for (int i4 = 0; i4<n; i4++) {
						for (int i5 = 0; i5<n; i5++) {
							for (int i6 = 0; i6<n; i6++) {
								for (int i7 = 0; i7<n; i7++) {
									for (int i8 = 0; i8<n; i8++) {
										for (int i9 = 0; i9<n; i9++) {
											for (int i10 = 0; i10<n; i10++) {
												for (int i11 = 0; i11<n; i11++) {
													for (int i12 = 0; i12<n; i12++) {
														currentset.setpiece[0] = allarmor[0][i0];
														currentset.setpiece[1] = allarmor[1][i1];
														currentset.setpiece[2] = allarmor[2][i2];
														currentset.setpiece[3] = allarmor[3][i3];
														currentset.setpiece[4] = allarmor[4][i4];
														currentset.setpiece[5] = allarmor[5][i5];
														currentset.setpiece[6] = allarmor[6][i6];
														currentset.setpiece[7] = allarmor[7][i7];
														currentset.setpiece[8] = allarmor[8][i8];
														currentset.setpiece[9] = allarmor[9][i9];
														currentset.setpiece[10] = allarmor[10][i10];
														currentset.setpiece[11] = allarmor[11][i11];
														currentset.setpiece[12] = allarmor[12][i12];

														double currentlife = life(currentset);
														double currentehp = ehp(currentset);
														double currentdmg = dmgstr(currentset);

														if (currentehp>minehp) {
															if (dmgexu(currentset)>dmgexu(currentbest)) {
																currentbest = currentset;
																//cout << dmg(currentbest) << endl;
																best[0] = i0;
																best[1] = i1;
																best[2] = i2;
																best[3] = i3;
																best[4] = i4;
																best[5] = i5;
																best[6] = i6;
																best[7] = i7;
																best[8] = i8;
																best[9] = i9;
																best[10] = i10;
																best[11] = i11;
																best[12] = i12;

															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	cout << "hi";
	for (int i = 0; i<13; i++) {
		cout << i << ": ";
		cout << best[i] << endl;
	}

	cout << "dmg: " << dmgstr(currentbest) << endl;
	cout << "ehp: " << ehp(currentbest) << endl;
	cout << "life " << life(currentbest) << endl;

	//ignore everything past this line
	cout << "ignore this:" << endl;
	double vitality = 1000;
	for (int i = 0; i<13; i++) {
		vitality += currentbest.setpiece[i].stats[3];
	}
	double toughness = 1000;
	for (int i = 0; i<13; i++) {
		toughness += currentbest.setpiece[i].stats[4];
	}

	double power = 1000;
	for (int i = 0; i<13; i++) {
		power += currentbest.setpiece[i].stats[0];
	}
	power += 0.06*toughness + 0.04*vitality;
	power += 175;
	double precision = 1000;
	for (int i = 0; i<13; i++) {
		precision += currentbest.setpiece[i].stats[1];
	}
	double ferocity = 0;
	for (int i = 0; i<13; i++) {
		ferocity += currentbest.setpiece[i].stats[2];
	}

	cout << power << endl;
	cout << precision << endl;
	cout << ferocity << endl;
	cout << vitality << endl;
	cout << toughness << endl;

	for (int i = 0; i < 13; i++) {
		for (int j = 0; j < 5; j++) {
			cout << currentbest.setpiece[i].stats[j] << " ";
		}
		cout << endl;
	}
	cin.ignore();
	cin.get();
	cout << "end" << endl;


}