from glbl import *
from runes import *
from statdistr import *

class ArmorPiece:
    def __init__(self,stat_numbers,stats,prefix_name,armor_name):
        # stats will indicate what stats the stat_numbers represent
        # these willbe "enum" values
        self.stat_numbers = [0,0,0,0,0,0,0,0,0]
        for i in range(len(stats)):
            self.stat_numbers[stats[i]] = stat_numbers[i]
        self.prefix_name = prefix_name
        self.armor_name = armor_name

    def __str__(self):
        return str(self.stats) + " " + str(self.stat_numbers) + " " + self.prefix_name + " " + self.armor_name

# Will contain a list of armorpieces aswell as runes/trinkets
class ArmorSet():
    def __init__(self,armor_pieces): # TODO: trinkets
        #set will have all the stats on it
        # for each of the stats (numbered 1-9)
        self.all_stats = [0,0,0,0,0,0,0,0,0]
        for i in range(9):
            for armor_piece in armor_pieces:
                self.all_stats[i] += armor_piece.stat_numbers[i]
        self.armor_pieces = armor_pieces

    def applyRunes(self,runeset):
        self.all_stats = [all_stats[i]+runeset.stat_numbers[i] for i in range(9)]
        # start counting percentage bonuses

# ====================Constructing all armor pieces=====================
# Helmets
all_helmets = [ArmorPiece(three_stat_helmet,prefix[0],prefix[1],"helmet") for prefix in all_3_attr_prefixes]+\
[ArmorPiece(four_stat_helmet,prefix[0],prefix[1],"helmet") for prefix in all_4_attr_prefixes]+[ArmorPiece(celestial_helmet,celestial,"celestial","helmet")]
# Chestpieces
all_chests = [ArmorPiece(three_stat_chest,prefix[0],prefix[1],"chest") for prefix in all_3_attr_prefixes]+\
[ArmorPiece(four_stat_chest,prefix[0],prefix[1],"chest") for prefix in all_4_attr_prefixes]+[ArmorPiece(celestial_chest,celestial,"celestial","chest")]
# Shoulders
all_shoulders = [ArmorPiece(three_stat_shoulders,prefix[0],prefix[1],"shoulders") for prefix in all_3_attr_prefixes]+\
[ArmorPiece(four_stat_shoulders,prefix[0],prefix[1],"shoulders") for prefix in all_4_attr_prefixes]+[ArmorPiece(celestial_shoulders,celestial,"celestial","shoulders")]
# Gloves
all_gloves = [ArmorPiece(three_stat_gloves,prefix[0],prefix[1],"gloves") for prefix in all_3_attr_prefixes]+\
[ArmorPiece(four_stat_gloves,prefix[0],prefix[1],"gloves") for prefix in all_4_attr_prefixes]+[ArmorPiece(celestial_gloves,celestial,"celestial","gloves")]
# Leggings
all_leggings = [ArmorPiece(three_stat_leggings,prefix[0],prefix[1],"leggings") for prefix in all_3_attr_prefixes]+\
[ArmorPiece(four_stat_leggings,prefix[0],prefix[1],"leggings") for prefix in all_4_attr_prefixes]+[ArmorPiece(celestial_leggings,celestial,"celestial","leggings")]
# Boots
all_boots = [ArmorPiece(three_stat_boots,prefix[0],prefix[1],"boots") for prefix in all_3_attr_prefixes]+\
[ArmorPiece(four_stat_boots,prefix[0],prefix[1],"boots") for prefix in all_4_attr_prefixes]+[ArmorPiece(celestial_boots,celestial,"celestial","boots")]
# Weapon 1H
all_weapons_1h = [ArmorPiece(three_stat_weapon_1h,prefix[0],prefix[1],"weapon_1hweapon_1h") for prefix in all_3_attr_prefixes]+\
[ArmorPiece(four_stat_weapon_1h,prefix[0],prefix[1],"weapon_1hweapon_1h") for prefix in all_4_attr_prefixes]+[ArmorPiece(celestial_weapon_1h,celestial,"celestial","weapon_1h")]
# Weapon 2H
all_weapons_2h = [ArmorPiece(three_stat_weapon_2h,prefix[0],prefix[1],"weapon_2h") for prefix in all_3_attr_prefixes]+\
[ArmorPiece(four_stat_weapon_2h,prefix[0],prefix[1],"weapon_2h") for prefix in all_4_attr_prefixes]+[ArmorPiece(celestial_weapon_2h,celestial,"celestial","weapon_2h")]

# ====================Constructing all trinkets=====================
# IMPORTANT: many illegal combo's here, check those
